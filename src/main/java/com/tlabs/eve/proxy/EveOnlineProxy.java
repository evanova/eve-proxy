package com.tlabs.eve.proxy;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cache.CacheConstants;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;


public final class EveOnlineProxy extends RouteBuilder {
    private static final String HTTP_AGENT = "User-Agent";

	private static class HeaderProcessor implements Processor {

		@Override
		public void process(Exchange exchange) throws Exception {
			HttpServletRequest request = exchange.getIn().getBody(HttpServletRequest.class);
			exchange.getIn().setHeader("CamelHttpClientHost", request.getRemoteAddr());
			//TODO get the proxy cache dir from configuration
			String filename =
				System.getProperty("user.dir") + "/.proxy/" +
				StringUtils.replace(exchange.getIn().getHeader(Exchange.HTTP_PATH, String.class), ".xml.aspx", "-") +
				exchange.getIn().getMessageId() +
				".xml";
			exchange.getIn().setHeader(Exchange.FILE_NAME, filename);
			exchange.setProperty(CacheConstants.CACHE_KEY, exchange.getIn().getHeader(CacheConstants.CACHE_KEY));
			exchange.setProperty(Exchange.HTTP_QUERY, exchange.getIn().getHeader(Exchange.HTTP_QUERY));
			exchange.setProperty(HTTP_AGENT, exchange.getIn().getHeader(HTTP_AGENT));
		}
	}

	private static class OutgoingHTTPProcessor implements Processor {
		@Override
		public void process(Exchange exchange) throws Exception {
			exchange.getIn().setHeader(Exchange.HTTP_METHOD, exchange.getProperty(Exchange.HTTP_METHOD));
			if ("POST".equals(exchange.getIn().getHeader(Exchange.HTTP_METHOD, String.class))) {
				exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/x-www-form-urlencoded");
			}
			exchange.getIn().setHeader(Exchange.HTTP_QUERY, exchange.getProperty(Exchange.HTTP_QUERY));
			exchange.getIn().setHeader(HTTP_AGENT, exchange.getProperty(HTTP_AGENT));
		}
	}

	public void configure() {

		configureRoute("api", "https://api.eveonline.com");
		configureRoute("test", "https://api.testeveonline.com");
		configureRoute("central", "http://api.eve-central.com");
		configureRoute("images", "https://image.eveonline.com", new Processor() {
			public void process(Exchange ex) throws Exception {
				InputStream in = ex.getIn().getBody(InputStream.class);

				ByteArrayOutputStream out = new ByteArrayOutputStream();
				IOUtils.copy(in, out);
				in.close();
				out.flush();
				out.close();
				ex.getIn().setBody(out.toByteArray());
			}
		});
	}

	private void configureRoute(String prefix, String uri) {
		configureRoute(prefix, uri, new Processor() {
			public void process(Exchange exchange) throws Exception {
                exchange.getIn().setBody(exchange.getIn().getBody(String.class));
			}
		});
	}

	private void configureRoute(final String prefix, final String uri, final Processor bodyProcessor) {
		final Endpoint cache = cache(prefix);
		final Endpoint file = file(prefix);

		from(bind(prefix))
		.threads(5, 10)
		.process(new HeaderProcessor())
		.log(LoggingLevel.INFO, EveOnlineProxy.class.getName() + "." + prefix, "From: ${header.CamelHttpClientHost}/${header.CamelHttpUri}${header.CamelHttpQuery}")
		.setHeader(CacheConstants.CACHE_OPERATION, constant(CacheConstants.CACHE_OPERATION_GET))
	    .to(cache)

	    //Cache check
	    .choice().when(header(CacheConstants.CACHE_ELEMENT_WAS_FOUND).isNull())
	        //cache miss
	    	.log(LoggingLevel.INFO, EveOnlineProxy.class.getName() + "." + prefix, "Cache miss: ${header.CamelHttpUri}")
	    	.process(new OutgoingHTTPProcessor())
	        .to(uri(uri))
		        .choice().when(header(Exchange.HTTP_RESPONSE_CODE).isEqualTo(500))
		        	.log(LoggingLevel.INFO, EveOnlineProxy.class.getName() + "." + prefix, "HTTP ${header.CamelHttpResponseCode}: ${header.CamelHttpUri}")
		        .when(header(Exchange.HTTP_RESPONSE_CODE).isEqualTo(404))
		        	.log(LoggingLevel.INFO, EveOnlineProxy.class.getName() + "." + prefix, "HTTP ${header.CamelHttpResponseCode}: ${header.CamelHttpUri}")
		        .otherwise()
		        	.log(LoggingLevel.INFO, EveOnlineProxy.class.getName() + "." + prefix, "Cache add: ${header.CamelHttpUri}")
		        	.process(bodyProcessor)
		        	.setHeader(CacheConstants.CACHE_OPERATION, constant(CacheConstants.CACHE_OPERATION_ADD))
		        	.setHeader(CacheConstants.CACHE_KEY, property(CacheConstants.CACHE_KEY))
		        	.process(bodyProcessor)
		        	.to(cache, file)
		        .endChoice()
	        .otherwise()//cached
	        	.log(LoggingLevel.INFO, EveOnlineProxy.class.getName() + "." + prefix, "Cache hit: ${header.CamelHttpUri}")
	    .end();//cache check
	}

	private Endpoint cache(String prefix) {
		return 	getContext().getEndpoint(
			"cache://" + prefix + "?maxElementsInMemory=50&overflowToDisk=true&eternal=true&diskPersistent=false");
	}

	private Endpoint uri(String uri) {
		return getContext().getEndpoint(
			uri + "?bridgeEndpoint=true&httpBindingRef=proxy.binding&disableStreamCache=true&throwExceptionOnFailure=false");
	}

	private Endpoint bind(String prefix) {
	    final String bind = System.getProperty("com.tlabs.eve.proxy.bind", "0.0.0.0:8080");
		return getContext().getEndpoint(
			"jetty:http://" + bind + "/" + prefix + "?matchOnUriPrefix=true&httpBindingRef=proxy.binding&enableMultipartFilter=false&traceEnabled=false");
	}

	private Endpoint file(String prefix) {
		return getContext().getEndpoint(
			"file:?autoCreate=true&delete=false");
	}
}

