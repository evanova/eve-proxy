package com.tlabs.eve.proxy;


import org.junit.Assert;
import org.junit.Test;

public class ProxyTest extends HttpClientTest {
	
	private static final String PROXY = "http://localhost:8080";
	
	@Test(timeout=10000)
	public void testCharacterImage() {
		Assert.assertNotNull(get(PROXY + "/images/Character/90808929_128.jpg"));
	}
	

}
